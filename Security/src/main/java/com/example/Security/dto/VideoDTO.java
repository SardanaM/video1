package com.example.Security.dto;

import com.example.Security.models.Video;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)

public class VideoDTO {
    private Long id;
    private String name;
    private String path;
    private String uploader;
    private Date date;



    public VideoDTO(Long id, String name, String path, String uploader, Date date) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.uploader = uploader;
        this.date = date;
    }

    public VideoDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUploader() {
        return uploader;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }



}
