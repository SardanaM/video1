package com.example.Security.models;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@Builder
//@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User implements UserDetails{


    private  List<String> videos = new ArrayList<>();
    private  boolean isAdmin;

    private int uploadCount = 0;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    public User() {

    }

    public void setUploadCount(int uploadCount) {
        this.uploadCount = uploadCount;
    }
    public int getUploadCount(User user) {
        return uploadCount++;
    }


    public User(String username) {
        this.username = username;
        this.isAdmin = username.equals("admin");
    }

    public void addVideo(String videoPath) {
        videos.add(videoPath);
        uploadCount++;
    }



    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


   public boolean isAdmin() {
        return isAdmin;
    }

    public int getUploadCount() {
        return uploadCount;
    }

    public List<String> getVideos() {
        return videos;
    }}













