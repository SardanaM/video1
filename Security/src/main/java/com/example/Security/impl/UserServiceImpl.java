package com.example.Security.impl;

import com.example.Security.models.User;
import com.example.Security.models.Video;
import com.example.Security.repository.UserRepository;
import com.example.Security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public void addVideo(User user,String videoPath) {
        user.addVideo(videoPath);
        userRepository.save(user);
    }

    @Override
    public int getUploadCount(User user) {
        return user.getUploadCount(); // Получаем значение uploadCount из объекта User
    }




}





