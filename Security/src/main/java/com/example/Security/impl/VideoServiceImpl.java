package com.example.Security.impl;
import com.example.Security.dto.VideoDTO;
import com.example.Security.models.User;
import com.example.Security.models.Video;
import com.example.Security.repository.UserRepository;
import com.example.Security.repository.VideoRepository;
import com.example.Security.service.VideoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;



@Service
public class VideoServiceImpl implements VideoService {

    private static final int MAX_UPLOADS = 2;
    public VideoRepository videoRepository;
    public UserRepository userRepository;
//    @Override
//    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {
//        try {
//            Video video = new Video();
//            video.setVideo(file.getBytes()); // Сохраняем байты изображения в объекте Post
//            videoRepository.save(video);
//            return ResponseEntity.ok("File uploaded successfully");
//        } catch (IOException e) {
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Could not upload the file");
//        }

    private boolean isVideo(MultipartFile file) {
        String filename = file.getOriginalFilename();
        return StringUtils.hasText(filename) && (filename.endsWith(".mp4") || filename.endsWith(".avi"));
    }

    @Override
    public List<VideoDTO> getAllvideos() {
        List<Video> videos = videoRepository.findAll();
        return videos.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public Video getVideoById(Long id) {
        Optional<Video> videoOptional = videoRepository.findById(id);
        return videoOptional.orElse(null);
//
//        if (videoOptional.isPresent()) {
//            Video video = videoOptional.get();
//            return video;
    }

//        return null;
//    }

    private VideoDTO convertToDTO(Video video) {
        VideoDTO dto = new VideoDTO();
        dto.setId(video.getId());
        dto.setName(video.getName());
        dto.setPath(video.getPath());
        dto.setUploader(video.getUploader());
        dto.setDate(video.getDate());
        return dto;
    }

    @Override
    public ResponseEntity<String> uploadFile(
            @AuthenticationPrincipal UserDetails userDetails,
            //@PathVariable String username,
            @RequestParam("file") MultipartFile file
    ) {
        if (userDetails == null) {
            // Handle unauthenticated access
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized access");
        }
        Optional<User> optionalUser = userRepository.findByEmail(userDetails.getUsername());
       // User user = users.get(username);// Получаем текущего пользователя

        return optionalUser.map(user -> {
        if (user.getUploadCount() < MAX_UPLOADS && isVideo(file)) {
            try {
                String videoPath = "uploads/" + user.getEmail() + "/" + file.getOriginalFilename();
                file.transferTo(new File(videoPath)); // Переносим файл

                Video video = new Video();
                video.setVideo(file.getBytes()); // Сохраняем байты видео в объекте Video
                videoRepository.save(video); // Сохраняем информацию о видео

                user.addVideo(videoPath); // Добавляем информацию о видео пользователю

                return ResponseEntity.ok("File uploaded successfully");
            } catch (IOException e) {
                // Обработка ошибки, если произошла ошибка при переносе файла или сохранении в базу данных
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Could not upload the file");
            }
        } else {
            // Обработка ошибки, например, если условия загрузки не выполнены
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Could not upload the file");
        }}).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found"));
    }












}
