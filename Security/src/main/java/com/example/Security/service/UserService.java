package com.example.Security.service;

import com.example.Security.models.User;
import com.example.Security.models.Video;

import java.util.List;

public interface UserService {


    void addVideo(User user, String videoPath);

    int getUploadCount(User user);




}
