package com.example.Security.service;

import com.example.Security.dto.VideoDTO;
import com.example.Security.models.Video;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface VideoService {



    ResponseEntity<String> uploadFile( @AuthenticationPrincipal UserDetails userDetails, @RequestParam("file") MultipartFile file);

    List<VideoDTO> getAllvideos();


    Video getVideoById(Long id);
}
