package com.example.Security.controllers;

import com.example.Security.dto.VideoDTO;
import com.example.Security.models.Video;
import com.example.Security.repository.VideoRepository;
import com.example.Security.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/videos")

public class VideoController {
    private VideoService videoService;

    @Autowired
    public VideoController(VideoService videoService) {
        this.videoService = videoService;

    }
    @PostMapping("/upload")
    public ResponseEntity<String> uploadFile( @AuthenticationPrincipal UserDetails userDetails,@RequestParam("file") MultipartFile file) {
        return videoService.uploadFile(userDetails,file);
    }

    @GetMapping("/upload/all")
    public List<VideoDTO> getAllvideos() {
        return videoService.getAllvideos();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Resource> getVideoById(@PathVariable(name = "id") Long id) {
        Video video = videoService.getVideoById(id);
        if (video == null) {
            return ResponseEntity.notFound().build();
        }
        ByteArrayResource videoResource = new ByteArrayResource(video.getVideo());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + video.getName() + "\"")
                .contentLength(video.getVideo().length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(videoResource);
    }}


